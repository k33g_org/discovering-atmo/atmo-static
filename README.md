# Subo template

### 🤚 To begin, click on the **Gitpod** button: [![Open in GitPod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/k33g_org/discovering-atmo/subo-template)

### 🖐 To force the rebuild of the image, click on this button: [![Open in GitPod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#imagebuild/https://gitlab.com/k33g_org/discovering-atmo/subo-template)

> - That will rebuild the image, without committing anything new to the `gitpod.dockerfile`.
> - Use case: Gitpod images might install packages that from time to time need update. The only way to update these packages is manually make a change in `gitpod.dockerfile`, which will initiate a rebuild to the image (and get the latest updates).

```bash
subo create project services


```