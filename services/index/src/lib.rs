use suborbital::runnable::*;
use suborbital::req;
use suborbital::file;
use std::path::Path;

struct Index{}

impl Runnable for Index {
    fn run(&self, _: Vec<u8>) -> Result<Vec<u8>, RunErr> {
        suborbital::resp::content_type("text/html; charset=utf-8");
        let mut filename = req::url_param("file");

        //let extension = Path::new(&filename).extension().unwrap().to_ascii_lowercase().to_str().unwrap();


        if filename == "" {
            filename = req::state("file").unwrap_or_default();
            //filename = format!("index.html")
        }

        Ok(file::get_static(filename.as_str()).unwrap_or("failed".as_bytes().to_vec()))
    }
}


// initialize the runner, do not edit below //
static RUNNABLE: &Index = &Index{};

#[no_mangle]
pub extern fn _start() {
    use_runnable(RUNNABLE);
}
